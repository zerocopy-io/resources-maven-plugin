package io.zerocopy.json;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import javax.lang.model.element.Modifier;
import java.io.*;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;

@Mojo(name = "process-resources", threadSafe = true)
public class ResourceProcessorMojo extends AbstractMojo {
    @Parameter(name = "project", readonly = true, defaultValue = "${project}")
    private MavenProject project;

    @Parameter(name = "outputDirectory", defaultValue = "${project.build.directory}/generated-sources/java/")
    private File outputDirectory;

    @Parameter(name = "resourceBundlePrefixes", required = true)
    private List<File> resourceBundlePrefixes;

    @Parameter(name = "basePackage", required = true, defaultValue = "test")
    private String basePackage;

    @Parameter(name = "resourceClassName", required = true, defaultValue = "R")
    private String resourceClassName;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        String fileNameSuffix = ".properties";

        if (!basePackage.matches("([a-zA_Z_][\\.\\w]*)"))
            throw new MojoExecutionException("invalid package name: " + basePackage);

        //noinspection ResultOfMethodCallIgnored
        outputDirectory.mkdirs();
        if (!outputDirectory.isDirectory())
            throw new MojoExecutionException("Output path is not a directory: " + outputDirectory);

        project.addCompileSourceRoot(outputDirectory.getPath());

        for (File resourceBundlePrefix : resourceBundlePrefixes) {
            if (resourceBundlePrefix.isDirectory())
                throw new IllegalArgumentException();

            File parent = resourceBundlePrefix.getParentFile();
            String fileNamePrefix = resourceBundlePrefix.getName();

            if (parent == null)
                throw new IllegalArgumentException();
            if (fileNamePrefix.isEmpty())
                throw new IllegalArgumentException();

            File[] files = parent.listFiles();
            if (files != null)
                for (File file : files) {
                    if (file.getName().startsWith(fileNamePrefix) && file.getName().endsWith(fileNameSuffix)
                            && file.isFile()) {
                        String localeInfix = file.getName().substring(fileNamePrefix.length(), file.getName().length() - fileNameSuffix.length());
                        if (localeInfix.isEmpty()) {
                            //this is the default file defining all the valid/required keys

                            Map<String, String> resources = new TreeMap<String, String>();
                            {
                                Properties properties = new Properties();
                                try {
                                    FileInputStream propertiesInputStream = new FileInputStream(file);
                                    try {
                                        properties.load(propertiesInputStream);
                                    } finally {
                                        propertiesInputStream.close();
                                    }
                                } catch (IOException e) {
                                    throw new MojoFailureException("Failed to read resource file: " + file, e);
                                }
                                for (Map.Entry<Object, Object> entry : properties.entrySet())
                                    resources.put((String) entry.getKey(), (String) entry.getValue());
                            }

                            TypeSpec.Builder stringResourceClassBuilder = TypeSpec.classBuilder("string")
                                    .addModifiers(Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);

                            for (Map.Entry<String, String> entry : resources.entrySet()) {
                                FieldSpec.Builder fieldBuilder = FieldSpec.builder(TypeName.get(String.class), entry.getKey(), Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);
                                fieldBuilder.initializer("$S", entry.getKey());
                                stringResourceClassBuilder.addField(fieldBuilder.build());
                            }

                            TypeSpec resourceClass = TypeSpec.classBuilder(resourceClassName)
                                    .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
                                    .addType(stringResourceClassBuilder.build())
                                    .build();

                            JavaFile javaFile = JavaFile.builder(basePackage, resourceClass)
                                    .build();

                            try {
                                javaFile.writeTo(outputDirectory);
                            } catch (IOException e) {
                                throw new MojoFailureException("Failed to write key source file: " + resourceClass.name, e);
                            }
                        }
                    }
                }
        }
    }
}
